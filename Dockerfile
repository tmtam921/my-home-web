FROM node:14 AS frontend
WORKDIR /usr/src/app/
COPY ./client ./

FROM node:14 AS server
WORKDIR /usr/src/server
COPY ./server ./

FROM frontend AS frontend-dev
RUN npm install
EXPOSE 3801
CMD ["npm", "run", "start"]

FROM server AS server-dev
RUN npm install
EXPOSE 3802
CMD ["npm", "run", "dev"]

FROM frontend-dev AS frontend-prod
RUN ["npm", "run", "build"]

FROM server AS server-prod
RUN npm install
RUN npm run build
COPY --from=frontend-prod /usr/src/app/dist /usr/src/server/build/dist
EXPOSE 80
CMD ["npm", "run", "prod"]
