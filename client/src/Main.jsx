import React, {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import Fade from '@material-ui/core/Fade';
import {getInitData} from './util/apiCall';
import {setInitData} from './redux/common/commonActions';
import LoadingWrapper from './entry/LoadingWrapper';
import MainRouter from './entry/MainRouter';

const Main = () => {
    const dispatch = useDispatch();
    const [isInit, setIsInit] = useState(true);
    const history = useHistory();

    const pathChecker = (loggedIn) => {
        const {pathname} = window.location;
        const pathOfWelcome = (/^[/]welcome/).test(pathname) || pathname === '/';

        if (!loggedIn && !pathOfWelcome) {
            history.replace('/welcome');
        }
    };

    useEffect(() => {
        getInitData().then((res) => {
            pathChecker(true);
            dispatch(setInitData({
                isPrivateConnect: res.isPrivate,
                loggedIn: true,
                userIp: res.userIp,
            }));
            setIsInit(false);
        });
    }, []);

    if (isInit) {
        return <LoadingWrapper />;
    }
    return <MainRouter />;
};

export default Main;
