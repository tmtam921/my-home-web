const layout = {
    appBar: {
        height: 48,
        activeColor: '#ff7961',
        inactiveColor: '#fff',
    },
    root: {
        background: '#DEFCFF',
        height: '100vh',
        width: '100%',
    },
};

export default layout;
