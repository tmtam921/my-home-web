import {createMuiTheme} from '@material-ui/core/styles';

const theme = {
    palette: {
        // type: 'dark',
        primary: {
            light: '#757ce8',
            main: '#3f50b5',
            dark: '#002884',
            contrastText: '#fff',
        },
        secondary: {
            light: '#ff7961',
            main: '#f44336',
            dark: '#ba000d',
            contrastText: '#000',
        },
        info: {
            light: '#64b5f6',
            main: '#2196f3',
            dark: '#1976d2',
            contrastText: '#fff'
        }
    },
};

const themeObj = createMuiTheme(theme);

export default themeObj;
