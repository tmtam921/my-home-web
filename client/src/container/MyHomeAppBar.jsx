import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import makeStyles from '@material-ui/core/styles/makeStyles';
import AppBar from '@material-ui/core/AppBar';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import layout from '../constants/layout';

const useStyles = makeStyles({
    root: {
        boxShadow: '0 0 0 0',
        height: layout.appBar.height,
    },
    btBtn: {
        fontWeight: 'bold',
        fontSize: 18,
        paddingTop: 14,
    },
});

const MyHomeAppBar = () => {
    const history = useHistory();
    const classes = useStyles();
    const [currentPathname, setCurrentPathname] = useState(window.location.pathname);

    useEffect(() => {
        history.listen((location) => {
            setCurrentPathname(location.pathname);
        });
    });

    const handleChangePath = (path) => {
        if (path === currentPathname) return;
        history.push(path);
    };

    return (
        <Collapse in={!(currentPathname === '/welcome' || currentPathname === '/')}>
            <AppBar
                position="static"
                classes={{root: classes.root}}
            >
                <div style={{paddingLeft: 10}} >
                    <IconButton
                        style={{color: currentPathname === '/home' ? layout.appBar.activeColor : layout.appBar.inactiveColor}}
                        onClick={() => { handleChangePath('/home'); }}
                    >
                        <HomeIcon />
                    </IconButton>
                    <IconButton
                        classes={{root: classes.btBtn}}
                        style={{color: currentPathname === '/bt' ? layout.appBar.activeColor : layout.appBar.inactiveColor}}
                        onClick={() => { handleChangePath('/bt'); }}
                    >
                        BT
                    </IconButton>
                </div>
            </AppBar>
        </Collapse>
    );
};

export default MyHomeAppBar;
