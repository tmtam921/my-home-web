import React from 'react';
import {useHistory} from 'react-router-dom';
import makeStyles from '@material-ui/core/styles/makeStyles';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TabWrapper from '../../../layer/TabWrapper';
import BoxWrapper from '../../../layer/BoxWrapper';
import UserInformationBox from './UserInformationBox';

const useStyles = makeStyles({
    userInfoWrapper: {
        margin: 10,
        padding: 10,
        width: 450,
    },
    userInfoPaperRoot: {
        margin: 10,
        padding: 10,
    },
    btnWrapperRoot: {
        margin: 10,
        padding: 10,
    },
});
const DashboardApp = () => {
    const history = useHistory();
    const classes = useStyles();

    const handleChangePath = (path) => {
        history.push(path);
    };
    return (
        <TabWrapper>
            <Grid
                container
                space={3}
                justify="flex-start"
                alignItems="flex-start"
            >
                <Grid item xs={12} sm={4} >
                    <div className={classes.userInfoWrapper} >
                        <Paper
                            elevation={0}
                            classes={{root: classes.userInfoPaperRoot}}
                        >
                            <UserInformationBox />
                        </Paper>
                    </div>
                </Grid>
                <Grid item xs={12} sm={8} >
                    <div className={classes.btnWrapperRoot} >
                        <Grid container >
                            <BoxWrapper
                                onClick={() => { handleChangePath('/bt'); }}
                            >
                                <span
                                    style={{
                                        fontSize: 20,
                                        fontWeight: 800,
                                    }}
                                >
                                    BT
                                </span>
                            </BoxWrapper>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
        </TabWrapper>
    );
};

export default DashboardApp;
