import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles({
    paperRoot: {
        margin: 10,
        padding: 10,
    },
});

const UserInformationBox = () => {
    const classes = useStyles();
    const {
        common: {init: {isPrivateConnect, userIp}},
    } = useSelector(store => store);

    return (
        <div>
            {userIp}
            <br />
            {`lan access: ${isPrivateConnect.toString()}`}
            <br />
            {navigator.userAgent}
        </div>
    );
};

export default UserInformationBox;
