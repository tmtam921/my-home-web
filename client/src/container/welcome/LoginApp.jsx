import React, {useState} from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles({
    cardRoot: {
        minWidth: 400,
        minHeight: 150
    },
});

const LoginApp = () => {
    const classes = useStyles();
    const [showPwd, setShowPwd] = useState(false);
    const [formError, setFormError] = useState(false);
    const [helperText, setHelperText] = useState(' ');
    const [inputValue, setInputValue] = useState('');

    const onChangeField = () => {

    };

    const onKeyPressField = () => {

    };

    const endAdornment = (
        <IconButton
            aria-label="toggle password visibility"
            onClick={() => {
                setShowPwd(!showPwd);
            }}
        >
            {showPwd ? <Visibility /> : <VisibilityOff />}
        </IconButton>
    );

    return (
        <Card className={classes.cardRoot}>
            <div
                style={{
                    paddingLeft: 75,
                    paddingtop: 20,
                    width: 250,
                }}
            >
                <FormControl key="login-pwd" error={formError} fullWidth>
                    <InputLabel>Password</InputLabel>
                    <Input
                        id="login-pwd"
                        value={inputValue}
                        onChange={onChangeField}
                        onKeyPress={onKeyPressField}
                        type={showPwd ? 'text' : 'password'}
                        endAdornment={endAdornment}
                        style={{alignItems: 'center'}}
                    />
                    <FormHelperText>{helperText}</FormHelperText>
                </FormControl>
            </div>
        </Card>
    );
};

export default LoginApp;
