import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {Transition} from 'react-transition-group';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Fab from '@material-ui/core/Fab';
import Fade from '@material-ui/core/Fade';
import LoginApp from './LoginApp';

const duration = 300;
const baseStyles = {
    transition: `top ${duration}ms ease-in-out`,
};
const transitionStyles = {
    entering: {top: 'calc(50vh - 125px)'},
    entered:  {top: 'calc(20vh - 10px)'},
    exiting:  {top: 'calc(20vh - 10px)'},
    exited:  {top: 'calc(50vh - 125px)'},
};

const useStyles = makeStyles({
    fabRoot: {
        positoin: 'absolute',
        width: 250,
        height: 250,
        left: 'calc(50vw - 125px)',
        fontSize: 20,
    },
});

const WelcomePage = () => {
    const [showLogin, setShowLogin] = useState(false);
    const {
        init: {loggedIn},
    } = useSelector(store => store.common);
    const history = useHistory();
    const classes = useStyles({showLogin});

    const welcomeOnClick = () => {
        // if (loggedIn) {
        //     setShowLogin(!showLogin);
        // } else {
        //     history.push('/home');
        // }
        history.push('/home');
    };

    return (
        <div>
            <Transition in={showLogin} timeout={0}>
                {state => (
                    <Fab
                        color='primary'
                        style={{
                            ...baseStyles,
                            ...transitionStyles[state],
                        }}
                        classes={{root: classes.fabRoot}}
                        onClick={welcomeOnClick}
                    >
                        Welcome~
                    </Fab>
                )}
            </Transition>
            <Fade
                mountOnEnter
                unmountOnExit
                in={showLogin}
                timeout={duration}
            >
                <div
                    style={{
                        position: 'absolute',
                        top: 'calc(50vh - 15px)',
                        left: 'calc(50vw - 200px)',
                    }}
                >
                    <LoginApp />
                </div>
            </Fade>
        </div>
    );
};

export default WelcomePage;
