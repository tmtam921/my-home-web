import React from 'react';
import {
    BrowserRouter,
    Route,
    Switch,
} from 'react-router-dom';
import MyHomeAppBar from '../container/MyHomeAppBar';
import WelcomePage from '../container/welcome/WelcomePage';
import BtApp from '../container/route/bt/BtApp';
import MediaApp from '../container/route/media/MediaApp';
import HomeApp from '../container/route/home/HomeApp';
import layout from '../constants/layout';

const MainRouter = () => {
    return (
        <BrowserRouter>
            <div style={{...layout.root}} >
                <MyHomeAppBar />
                <Route
                    render={({location}) => (
                        <Switch location={location}>
                            <Route path='/home' component={HomeApp} />
                            <Route path='/bt' component={BtApp} />
                            {/* <Route path='/media' component={MediaApp} /> */}
                            <Route default component={WelcomePage} />
                        </Switch>
                    )}
                />
            </div>
        </BrowserRouter>
    );
};

export default MainRouter;
