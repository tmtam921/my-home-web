import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import {ThemeProvider} from '@material-ui/core/styles';
import '@fontsource/roboto';
import Main from './Main';
import store from './redux/store';
import themeObj from './constants/theme';

// if (navigator.serviceWorker) {
//     navigator.serviceWorker.register('/btProxyServiceWorker.js').then(() => {
//         console.log('Service worker registered!');
//     });
// }

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter >
            <ThemeProvider theme={themeObj}>
                <Main />
            </ThemeProvider>
        </BrowserRouter>
    </Provider>,
	document.getElementById('root')
);