import React from 'react';
import PropTypes from 'prop-types';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
    paperRoot: {
        margin: 10,
        padding: 10,
        width: 100,
        height: 100,
        background: 'white',
    },
});

const BoxWrapper = (props) => {
    const classes = useStyles();

    return (
        <Grid item xs={12} sm={3} >
            <Button
                classes={{root: classes.paperRoot}}
                onClick={props.onClick}
            >
                {props.children}
            </Button>
        </Grid>
    );
};

BoxWrapper.propTypes = {
    onClick: PropTypes.func.isRequired,
    children: PropTypes.element.isRequired,
};

export default BoxWrapper;
