import React from 'react';
import PropTypes from 'prop-types';

const TabWrapper = (props) => {
    return (
        <div
            style={{
                width: '100%',
                height: 'calc(100% - 48px)',
                overflow: 'auto',
            }}
        >
            {props.children}
        </div>
    );
};

TabWrapper.propTypes = {
    children: PropTypes.element.isRequired,
};

export default TabWrapper;
