const Constants = {
    SET_IS_PRIVATE_CONNECT: 'SET_IS_PRIVATE_CONNECT',
    SET_INIT_DATA: 'SET_INIT_DATA',
};

export default Constants;
