import {configureStore} from '@reduxjs/toolkit';
import {createLogger} from 'redux-logger';
import ReduxThunk from 'redux-thunk';
import commonReducer from './common/commonReducer';

const middleware = [ReduxThunk];

if (process.env.NODE_ENV === 'development') {
    middleware.push(createLogger({
        level: 'info',
        collapsed: false,
        logger: console,
    }));
}

export default configureStore({
    reducer: {
        common: commonReducer,
    },
    middleware,
});
