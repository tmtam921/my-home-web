export const checkIpIsPrivate = async () => {
    const PATH = '/api/check-client-ip';
    const opt = {
        method: 'GET',
        credentials: 'include',
    };

    const res = await fetch(PATH, opt);
    const ctx = await res.json();

    return ctx;
}

export const getInitData = async () => {
    const PATH = '/api/get-init-data';
    const opt = {
        method: 'GET',
        credentials: 'include',
    };

    const res = await fetch(PATH, opt);
    const ctx = await res.json();

    return ctx;
}
