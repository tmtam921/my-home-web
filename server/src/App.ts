import express from 'express';
import path from 'path';
import morgan from 'morgan';
import session, {SessionOptions} from 'express-session';
import mongoose from 'mongoose';
import connectMongo from 'connect-mongo';
import ApiRouter from './routers/ApiRouter';
import {makeId} from './util/tools';
import constList from './const/util';

const MongoStore = connectMongo(session);

class App {
    public app: express.Application;
    private DB_PATH = process.env.NODE_ENV === 'production' ?
        `mongodb://${constList.DATABASE_PATH}/prod` : `mongodb://${constList.DATABASE_PATH}/dev`;

    constructor() {
        this.app = express();
        this.middlewareSetup();
        this.assetSetup();
        this.routerSetup();

        const sessionOpt: SessionOptions = {
            secret: `${makeId(32)}`,
            resave: false,
            saveUninitialized: false,
            cookie: {
                sameSite: 'lax',
                maxAge: 1000 * 60 * 60 * 12,
            },
        }

        if (process.env.NODE_ENV === 'production') {
            this.app.get('/*', (req, res) => {
                res.sendFile(path.join(__dirname, 'dist', 'index.html'));
            });
            sessionOpt.cookie = {
                ...sessionOpt.cookie,
                secure: true,
            };
        }
        this.connectDb(sessionOpt);
    }

    // private sessionChecker = (req, res, next) => {
    //     console.log('----- session checker);
    //     next();
    // }

    private routerSetup(): void {
        const apiRouter = new ApiRouter(mongoose.connection);
        this.app.use(apiRouter.getPath(), apiRouter.getRouter());
    }

    private assetSetup(): void {
        this.app.use(express.static(path.join(__dirname, 'dist')));
    }

    private middlewareSetup(): void {
        this.app.use(morgan('dev'));
    }

    private setupSession(opt: SessionOptions): void {
        console.log('---- setup session');
        this.app.use(session(opt));
    }

    private async connectDb(sessionOpt: SessionOptions) {
        console.log('---- connecting to database');
        mongoose
            .connect(this.DB_PATH, {
                user: constList.DATABASE_ACC_USER_DEV,
                pass: constList.DATABASE_ACC_PWD_DEV,
                useNewUrlParser: true,
                useUnifiedTopology: true,
            })
            .then(() => {
                console.log('---- connected to database', mongoose.connection.readyState);
                (mongoose as any).Promise = global.Promise;

                this.setupSession({
                    ...sessionOpt,
                    store: new MongoStore({
                        mongooseConnection: mongoose.connection,
                    }),
                });
            })
            .catch((err) => {
                console.log('---- failed to connect database', err);
            });
    }
}

export default new App().app;
