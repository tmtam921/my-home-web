import app from './App';

const PORT = process.env.PORT || 3802;

app.listen(PORT, () => {
  console.log(`---- server is listening on port ${PORT} ----`);
});
