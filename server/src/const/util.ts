const CONST_LIST = {
    CHAR_LIST_ARRAY: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
    DATABASE_PATH: 'database-mongo:3803',
    DATABASE_ACC_USER_DEV: 'devAdmin',
    DATABASE_ACC_PWD_DEV: 'devPwd',
    DATABASE_ACC_USER_PROD: 'myHomeUser',
    DATABASE_ACC_PWD_PROD: 'myHomePwd',
    BT_WEB_UI_PATH: 'http://192.168.1.199:8801/',
};

export default CONST_LIST;
