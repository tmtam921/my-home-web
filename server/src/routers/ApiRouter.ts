
import Router from './Router.abstract';
import mongoose from 'mongoose';
import {checkIpIsPrivate} from '../util/tools';

class ApiRouter extends Router {
    private db;

    constructor(db: mongoose.Connection) {
        super();
        this.setRoutes();
        this.setPath('/api');
        this.db = db;
    }

    protected setRoutes(): void {
        this.getRouter()
            .route('/check-client-ip')
            .get((req, res) => {
                const remoteAddr = req.connection.remoteAddress?.split(':').pop() ?? '';
                res.json({isPrivate: checkIpIsPrivate(remoteAddr)});
            });

        this.getRouter()
            .route('/test-db-connection')
            .get((req, res) => {
                this.db.collection('test');
            });

        this.getRouter()
            .route('/get-init-data')
            .get((req, res) => {
                const remoteAddr = req.connection.remoteAddress?.split(':').pop() ?? '';
                const isPrivate = checkIpIsPrivate(remoteAddr);
                const loggedIn = false
                // if (isPrivate) {
                //     res.json({isPrivate, loggedIn});
                // } else {
                //     res.json({isPrivate, loggedIn});
                // }
                res.json({
                    isPrivate,
                    loggedIn,
                    userIp: remoteAddr
                });
            });
    }
}

export default ApiRouter;