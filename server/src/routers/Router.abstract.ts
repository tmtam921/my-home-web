import {Router as ExpressRouter} from "express";

abstract class Router {
    private path = '/api';
    private route: ExpressRouter = ExpressRouter();
    protected abstract setRoutes(): void;

    protected setPath(pathName: string): void {
        this.path = pathName;
    }

    public getPath(): string {
        return this.path;
    }

    public getRouter(): ExpressRouter {
        return this.route;
    }
}

export default Router;