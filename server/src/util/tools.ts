import {randomInt} from 'crypto';
import constList from '../const/util';

export function checkIpIsPrivate (ip: string): boolean {
    const ipArr = ip.split('.');
    if (ipArr[0] === '10') {
        return true;
    } else if (ipArr[0] === '192' && ipArr[1] === '168') {
        return true;
    } else if (ipArr[0] === '172') {
        const temp = parseInt(ipArr[1], 10);
        if (temp >= 16 && temp <= 31) {
            return true;
        }
    }
    return false
}

export function makeId (length: number = 12): string {
    // const random = Math.random().toString(36)
    let id = '';
    const {CHAR_LIST_ARRAY} = constList;
    const charsLength = constList.CHAR_LIST_ARRAY.length;
    for (let i = 0; i < length; i++) {
        const randomNum = randomInt(charsLength);
        id += CHAR_LIST_ARRAY.charAt(randomNum);
    }
    return id;
}
